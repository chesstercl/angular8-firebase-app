// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: 'AIzaSyAn_4S5_HqNO3iQH53TtQe8FBrKucHSRgo',
    authDomain: 'a8-firebase-app.firebaseapp.com',
    databaseURL: 'https://a8-firebase-app.firebaseio.com',
    projectId: 'a8-firebase-app',
    storageBucket: '',
    messagingSenderId: '787584760023',
    appId: '1:787584760023:web:0933acff5e269ba9'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
